package ch.usi.si.msde.edsl.assignment_03

import model.HttpRequestModel.*
import model.JsonModel.*
import model.AsyncContext
import scala.concurrent.Future
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap

object JsonDSL:

  // Do not touch this.
  def json(init: JsonObject ?=> Unit) =
    given o: JsonObject = JsonObject()
    init
    o

  extension (key: String)
    def ->(value: JsonValue)(using t: JsonObject) = t.add(key, value)

  def jsarray(values: JsonValue*): JsonValue = JsonArray(values: _*)

  // Implement the rest of the DSL from here.


end JsonDSL

object HttpRequestDSL:

  // ** Do not touch this **
  import JsonDSL._
  import AsyncContext.{ given, * }

  // ** Implement the DSL here **

end HttpRequestDSL

@main def exercise1_1() = 
  // DO NOT touch this
  import JsonDSL.{ given, * }

  // val jsonFragment1 = json {
  //   "name" -> "morpheus"
  //   "job" -> "leader"
  // }

  // val jsonFragment2 = json {
  //   "title" -> "The Matrix"
  //   "sequel" -> false
  //   "duration" -> 136
  //   "director" -> jsarray(json {
  //     "firstName" -> "Lana"
  //     "lastName" -> "Wachowski"
  //   }, json {
  //     "firstName" -> "Lilly"
  //     "lastName" -> "Wachowski"
  //   })
  // }

end exercise1_1

@main def exercise1_2() =

  // DO NOT touch this
  import JsonDSL.{ given, * }
  import HttpRequestDSL.{ given, * }
  import AsyncContext.{ given, * }

  // Exercise 1.2
  // val getRequest1: Future[Response] = get(https`://` "www.google.com" / "search")
  // val getRequest2 = get { https`://` "www.usi.ch" / "en" / "university" }
  // val getRequest3 = get { http`://` "usi.cch" }

  // val postRequest1 = post { 
  //   https `://` "reqres.in" / "api" / "users"
  // } withEntity json {
  //   "name" -> "morpheus"
  //   "job" -> "leader"
  // }

  // val postRequest2 = post { 
  //   https `://` "reqres.in" / "api" / "register"
  // } withEntity json {
  //   "email" -> "eve.holt@reqres.in"
  //   "password" -> "drowssap"
  // }

  // val postRequest3 = post { 
  //   https `://` "reqres.in" / "api" / "login"
  // } withEntity json {
  //   "email" -> "peter@klaven"
  // }

  // Do not touch this, just uncomment it.
  // executeInSequence(getRequest1,getRequest2,getRequest3,
  //   postRequest1, postRequest2, postRequest3)
end exercise1_2