package ch.usi.si.msde.edsl.assignment_03.model

import scala.collection.mutable.HashMap

object JsonModel:

  sealed trait JsonValue
  
  case class JsonObject(private val values: HashMap[String, JsonValue] = new HashMap[String, JsonValue]()) extends JsonValue:
    def add(key: String, value: JsonValue): Unit = 
      values += ((key, value))
    override def toString() = 
      val valueRep = values.map{ case (k,v) => s"\"${k}\": ${v}" }
      s"""{ ${ valueRep.mkString(", ") } }"""
  end JsonObject

  case class JsonArray(private val values: JsonValue*) extends JsonValue:
    override def toString() = 
      values.map{ case v => s"${v}" }.mkString("[", ", ", "]") 
  end JsonArray
  
  case class JsonString(val value: String) extends JsonValue:
    override def toString() = s"\"$value\""
  case class JsonNumber(val value: Double) extends JsonValue:
    override def toString() = s"$value"

  sealed trait JsonBoolean extends JsonValue
  
  case object JsonTrue extends JsonBoolean:
    override def toString() = "true"

  case object JsonFalse extends JsonBoolean:
    override def toString() = "false"

end JsonModel