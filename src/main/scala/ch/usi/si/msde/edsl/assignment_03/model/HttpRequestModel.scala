package ch.usi.si.msde.edsl.assignment_03.model

import scala.concurrent.Future
import scala.util.Success
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.HttpMethods
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ContentTypes

/**
 * A simple model for Http requests 
 */
object HttpRequestModel:
  
  // an given value that allows async computations on futures.
  import AsyncContext.ec

  /**
   * Boxes a Url.
   */
  case class URL(value: String):
    require(value != null && value.length() > 0, "uri must be non-null and non-empty")
  end URL
  
  /**
    * A trait modeling a request on a URL.
    */
  trait Request:
    val url: URL
  
  
  /**
    * A http get request.
    *
    * @param uri a uri.
    */
  case class GetRequest(url: URL) extends Request:
    /**
      * Performs the Get requests.
      * 
      * The given instance is available if you do not touch the imports.
      *
      * @param system 
      * @return a future of a response.
      */
    def perform()(using system: akka.actor.ClassicActorSystemProvider): Future[Response] = 
        Http().singleRequest(HttpRequest(uri = url.value)).map(new Response(_))
  end GetRequest

  case class PostRequest(url: URL, rawJsonEntity: String) extends Request:
    /**
      * Performs a Post request.
      * 
      * The given instance is available if you do not touch the imports.
      *
      * @param system 
      * @return a future of a response.
      */
    def perform()(using system: akka.actor.ClassicActorSystemProvider): Future[Response] = 
        Http().singleRequest(HttpRequest(method = HttpMethods.POST, uri = url.value, entity = HttpEntity(ContentTypes.`application/json`, rawJsonEntity))).map(new Response(_))

  end PostRequest

  /**
    * A view of a http response, including the status code, the contant types, and the headers.
    *
    * @param akkaResponse the complete http response.
    */
  class Response(private val akkaResponse: HttpResponse):
    lazy val statusCode = akkaResponse.status.intValue()
    lazy val headers: Seq[(String, String)] = akkaResponse.headers.map { header => header.name() -> header.value }
    lazy val contentType = akkaResponse.entity.contentType.mediaType.toString()
  
    override def toString = s"Response with code ${statusCode} and content type ${contentType}"
  end Response

  /**
    * Executes some futures in sequence, and then terminates.
    *
    * @param seq a sequence of futures.
    */
  def executeInSequence[T](seq: Future[T]*): Unit = 
    val transformedSeq = seq map { _.transform(t => Success(t)) }
    Future.sequence(transformedSeq) map { list =>
      list.foreach(println)
    } foreach { _ =>
      AsyncContext.system.terminate()
    }
  end executeInSequence
  
end HttpRequestModel
