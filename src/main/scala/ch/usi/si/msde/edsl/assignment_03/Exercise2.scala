package ch.usi.si.msde.edsl.assignment_03

import scala.concurrent.{Future}
import scala.util.{Try, Success, Failure}
import scala.util.Success
import model.HttpRequestModel._
import model.AssertionModel._
import model.AssertionExecutor
import scala.concurrent.Await

trait RequestAssertionDSL extends AssertionExecutor:

  // Do not touch this import
  import model.AsyncContext._

end RequestAssertionDSL

object Exercise2_1 extends RequestAssertionDSL with App:

  // Do not touch this
  import JsonDSL.{ given, * }
  import HttpRequestDSL.{ given, * }
  import model.AsyncContext.{ given, * }

  // Exercise 2.1: Basic respond/fail assertions

  // assert("a get on user 2 will respond with 200 OK") := eventually {
  //   get(https `://` "reqres.in" / "api" / "user" / "3" )
  // } will respond `with` statusCode === 200

  // assert("a get on a non-existing user will respond with 404") := eventually {
  //   get(https `://` "reqres.in" / "api" / "user" / "3" )
  // } will respond `with` statusCode === 404

  // assert("a GET on a non-existing domain will fail") := eventually {
  //   get(https `://` "www.usi.chh")
  // } will fail

  // assert("a login without password will respond with status code 400") := eventually {
  //   post { 
  //     https `://` "reqres.in" / "api" / "login"
  //   } withEntity json {
  //     "email" -> "peter@klaven"
  //   }
  // } will respond `with` statusCode === 400

  // do ** NOT ** remove this
  run()
end Exercise2_1

object Exercise2_2 extends App with RequestAssertionDSL:

  // Do not touch this
  import JsonDSL._
  import HttpRequestDSL._
  import model.AsyncContext._

  // assert("a get on a user will respond with 200 or 404") := eventually {
  //   get(https `://` "reqres.in" / "api" / "user" / "3" )
  // } will respond `with` statusCode === 404 or statusCode === 200

  // assert("a get on an existing user will return 200 OK and contentType application/json") := eventually {
  //   get(https `://` "reqres.in" / "api" / "user" / "2" )
  // } will respond `with` statusCode === 200 or statusCode === 404 and 
  //   contentType === "application/json" 

  // do ** NOT ** remove this
  run()
end Exercise2_2