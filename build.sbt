val AkkaVersion = "2.6.17"
val AkkaHttpVersion = "10.2.7"
val scala3Version = "3.1.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "assignment-03-template",
    version := "2021.1",

    scalaVersion := scala3Version,
    fork := true,
    // connectInput in run := true,
    libraryDependencies += "com.typesafe.akka" % "akka-actor-typed_2.13" % AkkaVersion,
    libraryDependencies += "com.typesafe.akka" % "akka-stream_2.13" % AkkaVersion,
    libraryDependencies += "com.typesafe.akka" % "akka-http_2.13" % AkkaHttpVersion
  )